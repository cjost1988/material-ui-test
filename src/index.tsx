import * as React from "react";
import * as ReactDOM from "react-dom";
import IntegrationAutosuggest from "./modules/IntegrationAutosuggest";

ReactDOM.render(
    <IntegrationAutosuggest />,
    document.getElementById('form')
);
