import * as React from 'react';
import Downshift from 'downshift';
import {withStyles} from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Popper from '@material-ui/core/Popper';
import Paper from '@material-ui/core/Paper';
import MenuItem from '@material-ui/core/MenuItem';
import InputAdornment from "@material-ui/core/InputAdornment/InputAdornment";
import {Search} from "@material-ui/icons";

const suggestions = [
    { label: 'Afghanistan' },
    { label: 'Aland Islands' },
    { label: 'Albania' },
    { label: 'Algeria' },
    { label: 'American Samoa' },
    { label: 'Andorra' },
    { label: 'Angola' },
    { label: 'Anguilla' },
    { label: 'Antarctica' },
    { label: 'Antigua and Barbuda' },
    { label: 'Argentina' },
    { label: 'Armenia' },
    { label: 'Aruba' },
    { label: 'Australia' },
    { label: 'Austria' },
    { label: 'Azerbaijan' },
    { label: 'Bahamas' },
    { label: 'Bahrain' },
    { label: 'Bangladesh' },
    { label: 'Barbados' },
    { label: 'Belarus' },
    { label: 'Belgium' },
    { label: 'Belize' },
    { label: 'Benin' },
    { label: 'Bermuda' },
    { label: 'Bhutan' },
    { label: 'Bolivia, Plurinational State of' },
    { label: 'Bonaire, Sint Eustatius and Saba' },
    { label: 'Bosnia and Herzegovina' },
    { label: 'Botswana' },
    { label: 'Bouvet Island' },
    { label: 'Brazil' },
    { label: 'British Indian Ocean Territory' },
    { label: 'Brunei Darussalam' },
];

function renderInput(inputProps: any) {
    const { InputProps, classes, ref, ...other } = inputProps;

    return (
        <TextField
            InputProps={{
                inputRef: ref,
                classes: {
                    root: classes.inputRoot,
                },
                ...InputProps,
            }}
            {...other}
        />
    );
}

function calculateLongestItemLength(items: Array<any>): number {
    let length: number = 0;

    items.forEach((item: any) => {
        if (item.label.length > length) {
            length = item.label.length;
        }
    });

    return length;
}

function renderSuggestion({suggestion, index, itemProps, highlightedIndex, selectedItem}: {suggestion: any, index: any, itemProps: any, highlightedIndex: any, selectedItem: any}) {
    const isHighlighted = highlightedIndex === index;
    const isSelected = (selectedItem || '').indexOf(suggestion.label) > -1;

    return (
        <MenuItem
            {...itemProps}
            key={suggestion.label}
            selected={isHighlighted}
            component="div"
            style={{
                fontWeight: isSelected ? 500 : 400,
            }}
        >
            {suggestion.label}
        </MenuItem>
    );
}

function getSuggestions(inputValue: any) {
    let count = 0;

    return suggestions.filter(suggestion => {
        return (!inputValue || suggestion.label.toLowerCase().indexOf(inputValue.toLowerCase()) !== -1);
    });
}

const styles: any = (theme: any) => ({
    root: {
        flexGrow: 1,
        height: 250,
    },
    container: {
        flexGrow: 1,
        position: 'relative',
    },
    paper: {
        position: 'absolute',
        zIndex: 1,
        marginTop: theme.spacing.unit,
        left: 0,
        right: 0,
    },
    chip: {
        margin: `${theme.spacing.unit / 2}px ${theme.spacing.unit / 4}px`,
    },
    inputRoot: {
        flexWrap: 'wrap',
    },
    divider: {
        height: theme.spacing.unit * 2,
    },
});

let popperNode1: any, popperNode2: any, popperNode3: any;
let outerElement1: Element, outerElement2: Element, outerElement3: Element;

function IntegrationDownshift(props: any) {
    const { classes } = props;

    return (
        <div className={classes.root}>
            <div style={{float: 'left'}}>
                <Downshift id="downshift-popper" onSelect={(selectedItem: any, stateAndHelpers: object) => {
                    console.log('Downshift::onSelect() ', selectedItem, stateAndHelpers);
                }}>
                    {({getInputProps, getItemProps, isOpen, inputValue, selectedItem, highlightedIndex, setState}: {getInputProps: any, getItemProps: any, isOpen: any, inputValue: any, selectedItem: any, highlightedIndex: any, setState: any}) => {
                        // inputValue = the current set value
                        // selectedItem = the selected item in the list


                        // onFocus: empty inputValue
                        // onBlur: inputValue = selectedItem
                        const
                            suggestions: Array<any> = getSuggestions(inputValue),
                            itemLength: number = isOpen ? calculateLongestItemLength(suggestions) : (selectedItem === null ? 15 : selectedItem.length);
                        return (
                        <div className={classes.container} style={{width: itemLength * 8 + 32}}>
                            <div className={classes.container} ref={(node: HTMLDivElement) => {outerElement1 = node;}}>
                                {renderInput({
                                    fullWidth: true,
                                    classes,
                                    InputProps: getInputProps({
                                        autoFocus: true,
                                        onFocus: () => setState({isOpen: true, inputValue: ''}), // overwrite open if the focus into the input is set
                                        startAdornment: isOpen && (
                                            <InputAdornment position="start">
                                                <Search />
                                            </InputAdornment>
                                        ),
                                    }),
                                    ref: (node: any) => {
                                        //popperNode1 = node;
                                    },
                                    placeholder: 'Produktgruppe suchen',
                                    label: 'Produktgruppe suchen',
                                })}
                            </div>
                            <Popper open={isOpen} anchorEl={outerElement1} placement="bottom-start">
                                <Paper square style={{width: outerElement1 ? outerElement1.clientWidth : undefined, maxHeight: 200, overflow: 'auto'}}>
                                    {suggestions.map((suggestion, index) =>
                                        renderSuggestion({
                                            suggestion,
                                            index,
                                            itemProps: getItemProps({ item: suggestion.label }),
                                            highlightedIndex,
                                            selectedItem,
                                        }),
                                    )}
                                </Paper>
                            </Popper>
                        </div>
                    )}}
                </Downshift>
            </div>
            <div style={{float: 'left', marginLeft: '10px'}}>
                <Downshift id="downshift-popper2">
                    {({getInputProps, getItemProps, isOpen, inputValue, selectedItem, highlightedIndex, setState}: {getInputProps: any, getItemProps: any, isOpen: any, inputValue: any, selectedItem: any, highlightedIndex: any, setState: any}) => {
                        const suggestions: Array<any> = getSuggestions(inputValue),
                            itemLength: number = isOpen ? calculateLongestItemLength(suggestions) : (selectedItem === null ? 15 : selectedItem.length);
                        return (
                        <div className={classes.container} style={{width: itemLength * 8 + 32}}>
                            <div className={classes.container} ref={(node: HTMLDivElement) => {outerElement2 = node;}}>
                                {renderInput({
                                    fullWidth: true,
                                    classes,
                                    InputProps: getInputProps({
                                        onFocus: () => setState({isOpen: true, inputValue: ''}), // overwrite open if the focus into the input is set
                                        startAdornment: isOpen && (
                                            <InputAdornment position="start">
                                                <Search />
                                            </InputAdornment>
                                        ),
                                    }),
                                    ref: (node: any) => {
                                        popperNode2 = node;
                                    },
                                    placeholder: 'Produkt suchen',
                                    label: 'Produkt suchen',
                                })}
                            </div>
                            <Popper open={isOpen} anchorEl={outerElement2} placement="bottom-start">
                                <Paper square style={{ width: outerElement2 ? outerElement2.clientWidth : undefined, maxHeight: 200, overflow: 'auto' }}>
                                    {suggestions.map((suggestion, index) =>
                                        renderSuggestion({
                                            suggestion,
                                            index,
                                            itemProps: getItemProps({ item: suggestion.label }),
                                            highlightedIndex,
                                            selectedItem,
                                        }),
                                    )}
                                </Paper>
                            </Popper>
                        </div>
                    )}}
                </Downshift>
            </div>
            <div style={{float: 'left', marginLeft: '10px'}}>
                <Downshift id="downshift-popper3">
                    {({getInputProps, getItemProps, isOpen, inputValue, selectedItem, highlightedIndex, setState}: {getInputProps: any, getItemProps: any, isOpen: any, inputValue: any, selectedItem: any, highlightedIndex: any, setState: any}) => {
                        const suggestions: Array<any> = getSuggestions(inputValue),
                            itemLength: number = isOpen ? calculateLongestItemLength(suggestions) : (selectedItem === null ? 15 : selectedItem.length);
                        return (
                        <div className={classes.container} style={{width: itemLength * 8 + 32}}>
                            <div className={classes.container} ref={(node: HTMLDivElement) => {outerElement3 = node;}}>
                                {renderInput({
                                    fullWidth: true,
                                    classes,
                                    InputProps: getInputProps({
                                        onFocus: () => setState({isOpen: true, inputValue: ''}), // overwrite open if the focus into the input is set
                                        startAdornment: isOpen && (
                                            <InputAdornment position="start">
                                                <Search />
                                            </InputAdornment>
                                        ),
                                    }),
                                    ref: (node: any) => {
                                        popperNode3 = node;
                                    },
                                    placeholder: 'Material suchen',
                                    label: 'Material suchen',
                                })}
                            </div>
                            <Popper open={isOpen} anchorEl={outerElement3} placement="bottom-start">
                                <Paper square style={{ width: outerElement3 ? outerElement3.clientWidth : undefined, maxHeight: 200, overflow: 'auto' }}>
                                    {suggestions.map((suggestion, index) =>
                                        renderSuggestion({
                                            suggestion,
                                            index,
                                            itemProps: getItemProps({ item: suggestion.label }),
                                            highlightedIndex,
                                            selectedItem,
                                        }),
                                    )}
                                </Paper>
                            </Popper>
                        </div>
                    )}}
                </Downshift>
            </div>
        </div>
    );
}

export default withStyles(styles)(IntegrationDownshift);
